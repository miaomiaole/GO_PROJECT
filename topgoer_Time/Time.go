package topgoer_Time

import (
	"fmt"
	"sync"
	"time"
)

// 时间函数打印
func TimeDemo(wg *sync.WaitGroup) {
	// 获取当前时间
	now := time.Now()
	//2022-04-30 17:21:43.28203 +0800 CST m=+0.000040543
	fmt.Println(now)

	// 时间戳转换
	unix_time := now.Unix()
	unix_Nano := now.UnixNano()
	unix_Milli := now.UnixMilli()
	fmt.Println("时间转时间戳", unix_time)
	fmt.Println("时间转时间戳", unix_Nano)
	fmt.Println("时间转时间戳", unix_Milli)

	changeTime := time.Unix(unix_time, 0)
	fmt.Println("时间戳转时间", changeTime)

	fmt.Println("时间间隔函数 ==========")
	// wg.Done()
	timeDuration()

	wg.Done()
}

// 时间间隔函数
func timeDuration() {
	now := time.Now()
	now.Add(time.Hour * 2)
	fmt.Println("定时器======")
	NewTimer(time.Second)
	// TickDemo(time.Second)
	//定时器后面方法不会执行
	fmt.Println("=====End=======")
}

func NewTimer(dur time.Duration) {
	// 包装的 tick
	timer := time.NewTimer(dur)
	timer.Reset(time.Second)
	fmt.Println(<-timer.C)
}

// 每多少间隔时间执行一次
func TickDemo(dur time.Duration) {
	// channel
	tick := time.Tick(dur)

	for task := range tick {
		// task 当前时间
		fmt.Println("task", task)

		str_time := formatTime(task, "2006-01-02 15:04:05")
		formatTime(task, "2006/01/02 15:04:05")
		formatTime(task, "2006-01/02 03:04:05")
		now_time := parseTime(str_time)
		fmt.Println("字符串转时间，", now_time)
	}
}

// 私有函数 内部调用
func formatTime(time time.Time, str string) string {
	format := time.Format(str)
	fmt.Println("时间转字符串：", format)
	return format
}
func parseTime(str string) time.Time {
	parse, err := time.Parse("2006-01-02 15:04:05", str)
	if err != nil {
		fmt.Println(err)
	}

	return parse
}
