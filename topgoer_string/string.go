package topgoer_string

import (
	"fmt"
	"strings"
)

// 以一个 或者连续多个空格切分
func Fields(str string) {
	fields := strings.Fields(str)
	for _, v := range fields {
		fmt.Println(v)
	}
}
