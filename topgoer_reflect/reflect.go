package topgoer_reflect

import (
	"fmt"
	"reflect"
)

//反射字段类型
func ReflectType(data interface{}) {
	t := reflect.TypeOf(data)
	fmt.Println("类型是：", t)
	kind := t.Kind()
	fmt.Println("当前数据类型", kind)

	switch kind {
	case reflect.Float64:
		fmt.Printf("a is float64\n")
	case reflect.String:
		fmt.Println("string")
	}
}

// 反射值类型
func ReflectValue(data interface{}) {
	value := reflect.ValueOf(data)
	fmt.Println(value)
	kind := value.Kind()
	fmt.Println("数据类型", kind)
	switch kind {
	case reflect.Float64:
		fmt.Println("a是：", value.Float())
	}
}
