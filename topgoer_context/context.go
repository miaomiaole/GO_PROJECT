package topgoer_context

import (
	"context"
	"fmt"
	"math/rand"
	"net/http"
	"sync"
	"time"
)

// 使用Context 同步信号  定义执行时间
func RunContext(dur time.Duration, dur2 time.Duration) {
	// 设置子线程执行时间
	ctx, cancelFunc := context.WithTimeout(context.Background(), dur)
	defer cancelFunc()
	// 实际执行时间
	go HandleContext(ctx, dur2)

	select {
	case <-ctx.Done():
		fmt.Println("RunContext", ctx.Err())
	}
}

func HandleContext(ctx context.Context, dur time.Duration) {
	select {
	// 到时间后调用
	case <-ctx.Done():
		fmt.Println("handle", ctx.Err())
		// 阻塞 等待dur后调用
	case <-time.After(dur):
		fmt.Println("Process request with", dur)
	}
}

// 携程 worker 每隔1秒钟打印
func worker(wg *sync.WaitGroup, ctx context.Context) {

	isFor := true
	for isFor {
		fmt.Println("正在循环打印")
		time.Sleep(time.Second)
		select {
		case <-ctx.Done():
			fmt.Println("ctx.Done")
			isFor = false
		}
	}
	// 没有其他携程通讯 永远不会执行
	fmt.Println("====worker 执行结束====")
	wg.Done()
}

func WorkContext(wg *sync.WaitGroup, ctx context.Context) {
	go worker(wg, ctx)
}

// 基于Server 使用Context
func indexHandle(w http.ResponseWriter, r *http.Request) {
	number := rand.Intn(2)
	if number == 0 {
		// 请求超时
		time.Sleep(time.Second * 10)
		fmt.Fprintln(w, "slow Response")
	}
	fmt.Fprint(w, "quick success")
}
