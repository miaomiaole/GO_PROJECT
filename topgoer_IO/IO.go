/**
     OS 操作
     os.Stdin：标准输入的文件实例，类型为*File
     os.Stdout：标准输出的文件实例，类型为*File
	 os.Stderr：标准错误输出的文件实例，类型为*File
*/
package topgoer_IO

import (
	"fmt"
	"io"
	"os"
)

// 监听键盘输入
func OSWriteString() {
	var buf [16]byte
	// stdin 监听键盘输入
	os.Stdin.Read(buf[:])

	os.Stdin.WriteString(string(buf[:]))
}

// 写文件
func WriteStirgFile(path string, str string) {
	fmt.Println("=======写文件操作=====")
	open, err := os.Create(path)
	if err != nil {
		fmt.Println("open file failed!, err:", err)
		return
	}
	defer open.Close()
	// 写文件

	open.WriteString(str)

}

//Append 写文件
func WriteStringAppend(path string) {
	fmt.Println("==== 追加写文件=====")

	open, err := os.Create(path)
	if err != nil {
		fmt.Println("打开文件异常，", err)
	}
	defer open.Close()

	var byeArr []byte

	var result []byte
	for {
		_, err_read := open.Read(byeArr)
		if err_read == io.EOF {
			break
		}
		if err_read != nil {
			fmt.Println("read file err ", err)
			return
		}
		result = append(result, byeArr...)
	}

	// 数组转字符串
	str := string(result)
	fmt.Println("读取文件已有数据，", str)

	_, err_write := open.WriteString(str + "这是追加写的内容")
	if err_write != nil {
		fmt.Println("写异常", err_write)
	}

}
