package topgoer_data_format

import (
	"encoding/json"
	"fmt"
)

type Person struct {
	Name   string `json:"name"`
	Hobby  string `json:"hobby"`
	Addree string `json:"addree"`
	Age    int    `json:"-"`
}

// Struct 转 json 字串
func TypeToJsonStr() {
	person := Person{"Abced", "rrrr", "", 12}
	marshal, err := json.Marshal(person)
	if err != nil {
		fmt.Println("json err ", err)
	}
	fmt.Println("字符串转换：" + string(marshal))

	indent, err_json := json.MarshalIndent(person, "", "  ")

	if err_json != nil {
		fmt.Println("json err ", err)
	}
	fmt.Println(string(indent))
}

func JsonStrToType() {
	str := `{"age":13,
             "name":"miaomiaole",
             "hobby":"---------",
             "addree":"2121212",
             "sex":"nan"}`
	var p Person
	err := json.Unmarshal([]byte(str), &p)
	if err != nil {
		fmt.Println("转换异常", err)
	}
	fmt.Println(p)
}
