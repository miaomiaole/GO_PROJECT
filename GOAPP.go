package main

import (
	"GO_PROJECT/topgoer_IO"
	"GO_PROJECT/topgoer_Strconv"
	"GO_PROJECT/topgoer_Time"
	"GO_PROJECT/topgoer_context"
	"GO_PROJECT/topgoer_data_format"
	"GO_PROJECT/topgoer_string"
	"context"
	"fmt"
	"sync"
	"time"
)

var wg sync.WaitGroup

func main() {
	wg.Add(2)
	go topgoer_Time.TimeDemo(&wg)

	topgoer_IO.OSWriteString()

	topgoer_IO.WriteStirgFile("a.txt", "miaomiaole")

	// topgoer_IO.WriteStringAppend("a.txt")

	topgoer_Strconv.StringToInt("121212", 10)

	// 模板请求
	// topgoer_service.HandleFuncHello(topgoer_service.SayHello)

	//var data = topgoer_http.RequestData{ "utf-8", "8", "1", "baidu", "golang 多级包导入", }

	//topgoer_http.RequestGet("http://www.baidu.com/s", data)

	// topgoer_http.HandleServer("/miaomiaole", topgoer_http.GetHandler)

	topgoer_context.RunContext(time.Second, 500*time.Millisecond)

	topgoer_context.RunContext(time.Second, 1500*time.Millisecond)

	cxt, cancelFunc := context.WithCancel(context.Background())

	topgoer_context.WorkContext(&wg, cxt)

	// 三秒后手动结束子携程
	time.Sleep(time.Second * 3)
	cancelFunc()
	wg.Wait()

	topgoer_data_format.TypeToJsonStr()

	topgoer_data_format.JsonStrToType()

	topgoer_string.Fields("Main Process End")

	fmt.Println("Main Process End")

}
