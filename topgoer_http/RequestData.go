package topgoer_http

type RequestData struct {
	Ie     string
	F      string
	Rsv_bp string
	Tn     string
	Wd     string
}
