/**
 * 基础数据类型转换
 */
package topgoer_Strconv

import (
	"fmt"
	"reflect"
	"strconv"
)

// string 与 int
func StringToInt(str string, num int) {
	atoi, err := strconv.Atoi(str)
	if err != nil {
		fmt.Println("转换异常", err)
	}
	// 判断Interface 类型
	//i := atoi.(int)
	// 通过反射判断类型
	of := reflect.TypeOf(atoi)
	if of.String() == "int" {
		fmt.Println("当前类型%T", atoi)
	}

	// 将整数类型转为字符串
	itoa := strconv.Itoa(num)

	fmt.Println("变量itoa %s 的类型是 %T", itoa, itoa)

}
