package topgoer_service

import (
	"fmt"
	"net/http"
	"text/template"
)

// 服务类方法
func HandleFuncHello(method interface{}) {
	methods := method.(func(w http.ResponseWriter, r *http.Request))
	http.HandleFunc("/miaomiaole", methods)
	err := http.ListenAndServe(":9090", nil)
	if err != nil {
		fmt.Println("HTTP server failed,err:", err)
		return
	}
}

func SayHello(w http.ResponseWriter, r *http.Request) {
	files, err := template.ParseFiles("topgoer_Template/static/hello.html")
	if err != nil {
		fmt.Println("create template failed, err:", err)
		return
	}
	files.Execute(w, "miaomiaole")
}

func sayHello2(w http.ResponseWriter, data interface{}) {

	files, err := template.ParseFiles("topgoer_Template/static/hello.html")
	if err != nil {
		fmt.Println("create template failed, err:", err)
		return
	}
	files.Execute(w, data)

}
